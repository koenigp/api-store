<?php

namespace App\Tests\Elasticsearch;

use App\Elasticsearch\DocumentExchanger;
use App\Entity\Product;
use App\Entity\Store;
use App\Repository\ProductRepository;
use App\Repository\StoreRepository;
use Elastica\Document;
use PHPUnit\Framework\TestCase;

class DocumentExchangerTest extends TestCase
{
    public function testFetchDocument()
    {
        $productMock = $this->getMockBuilder(Product::class)->getMock();
        $productRepository = $this->getMockBuilder(ProductRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productRepository->expects(self::any())
            ->method('find')
            ->willReturn($this->returnCallback(function($arg) use ($productMock){
                return '1' === $arg ? $productMock : null;
            }));

        $storeMock = $this->getMockBuilder(Store::class)->getMock();
        $storeRepository = $this->getMockBuilder(StoreRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $storeRepository->expects(self::any())
            ->method('find')
            ->willReturn($this->returnCallback(function($arg) use ($storeMock){
                return '1' === $arg ? $storeMock : null;
            }));

        $documentExchanger = new DocumentExchanger($productRepository, $storeRepository);

        self::assertInstanceOf(
            Document::class,
            $documentExchanger->fetchDocument(\App\Model\Product::class, '1')
        );
        self::assertNull(
            $documentExchanger->fetchDocument(\App\Model\Product::class, '2')
        );
        self::assertInstanceOf(
            Document::class,
            $documentExchanger->fetchDocument(\App\Model\Store::class, '1')
        );
        self::assertNull(
            $documentExchanger->fetchDocument(\App\Model\Store::class, '2')
        );
    }
}