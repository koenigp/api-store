<?php

namespace App\Tests\Service;

use App\Service\ManagerService;
use Doctrine\ORM\EntityManagerInterface;
use JoliCode\Elastically\Client;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;

class ManagerServiceTest extends TypeTestCase
{
    private $requestStackMock;
    private $busMock;
    private $elkMock;
    private $emMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->requestStackMock = $this->getMockBuilder(RequestStack::class)->disableOriginalConstructor()->getMock();
        $this->formFactoryMock = $this->getMockBuilder(FormFactoryInterface::class)->disableOriginalConstructor()->getMock();
        $this->busMock = $this->getMockBuilder(MessageBusInterface::class)->disableOriginalConstructor()->getMock();
        $this->elkMock = $this->getMockBuilder(Client::class)->disableOriginalConstructor()->getMock();
        $this->emMock = $this->getMockBuilder(EntityManagerInterface::class)->disableOriginalConstructor()->disableArgumentCloning()->getMock();
    }

    public function testAddManagerFailed()
    {
        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('isValid')
            ->willReturn(false);

        $this->formFactoryMock->expects(self::any())
            ->method('create')
            ->willReturn($formMock);

        $managerService = new ManagerService(
            $this->requestStackMock,
            $this->formFactoryMock,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertInstanceOf(
            Form::class,
            $managerService->addManager([])
        );
    }

    public function testAddManager()
    {
        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('isValid')
            ->willReturn(true);

        $this->formFactoryMock->expects(self::any())
            ->method('create')
            ->willReturn($formMock);

        $this->emMock->expects(self::any())
            ->method('flush')
            ->will($this->returnCallback(function($arg){
                $reflection = new \ReflectionClass($arg);
                $property = $reflection->getProperty('id');
                $property->setAccessible(true);
                $property->setValue($arg, 1);

                return $arg;
            }));

        $productService = new ManagerService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertSame(
            ['id' => 1],
            $productService->addManager([
                'firstName' => 'Morgane',
                'photo' => 'Sézalory'
            ])
        );
    }
}