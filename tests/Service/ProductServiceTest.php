<?php

namespace App\Tests\Service;

use App\Model\Product;
use App\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Index;
use Elastica\ResultSet;
use JoliCode\Elastically\Client;
use JoliCode\Elastically\Result;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

class ProductServiceTest extends TypeTestCase
{
    private $requestStackMock;
    private $busMock;
    private $elkMock;
    private $emMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->requestStackMock = $this->getMockBuilder(RequestStack::class)->disableOriginalConstructor()->getMock();
        $this->formFactoryMock = $this->getMockBuilder(FormFactoryInterface::class)->disableOriginalConstructor()->getMock();
        $this->busMock = $this->getMockBuilder(MessageBusInterface::class)->disableOriginalConstructor()->getMock();
        $this->elkMock = $this->getMockBuilder(Client::class)->disableOriginalConstructor()->getMock();
        $this->emMock = $this->getMockBuilder(EntityManagerInterface::class)->disableOriginalConstructor()->disableArgumentCloning()->getMock();
    }

    public function testAddProductFailed()
    {
        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('isValid')
            ->willReturn(false);

        $this->formFactoryMock->expects(self::any())
            ->method('create')
            ->willReturn($formMock);

        $productService = new ProductService(
            $this->requestStackMock,
            $this->formFactoryMock,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertInstanceOf(
            Form::class,
            $productService->addProduct([])
        );
    }

    public function testAddProduct()
    {
        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('isValid')
            ->willReturn(true);

        $this->formFactoryMock->expects(self::any())
            ->method('create')
            ->willReturn($formMock);

        $this->emMock->expects(self::any())
            ->method('flush')
            ->will($this->returnCallback(function($arg){
                $reflection = new \ReflectionClass($arg);
                $property = $reflection->getProperty('id');
                $property->setAccessible(true);
                $property->setValue($arg, 1);

                return $arg;
            }));

        $productService = new ProductService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertSame(
            ['id' => 1],
            $productService->addProduct([
                'name' => 'foo',
                'photo' => 'pics'
            ])
        );
    }

    public function testGetProductsNoResults()
    {
        $productService = new ProductService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        $result = $productService->getProducts([
            'offset' => 0,
            'limit' => 10,
        ]);

        self::assertInstanceOf(NotFoundHttpException::class, $result);
    }

    public function testGetProducts()
    {
        $hits = [
            'inner_hits' =>
                [
                   'stores' => [
                       'hits' => [
                           'hits' => [
                               [
                                   '_source' => [
                                       'name' => 'acme',
                                       'id' => 1,
                                       'stock' => 123
                                   ]
                               ]
                           ]
                       ]
                   ]
                ]
        ];
        $productModel = new \App\Model\Product();
        $productModel->id = 1;
        $productModel->photo = 'photo';
        $result = new Result($hits);
        $result->setModel($productModel);

        $resultSet = $this->getMockBuilder(ResultSet::class)->disableOriginalConstructor()->getMock();
        $resultSet->expects(self::atLeastOnce())
            ->method('count')
            ->willReturn(1);
        $resultSet->expects(self::atLeastOnce())
            ->method('getTotalHits')
            ->willReturn(1);
        $resultSet->expects(self::atLeastOnce())
            ->method('getResults')
            ->willReturn([$result]);

        $indexMock = $this->getMockBuilder(Index::class)->disableOriginalConstructor()->getMock();
        $indexMock->expects(self::atLeastOnce())
            ->method('search')
            ->willReturn($resultSet);

        $this->elkMock->expects(self::atLeastOnce())
            ->method('getIndex')
            ->willReturn($indexMock);

        $this->requestStackMock->expects(self::atLeastOnce())
            ->method('getCurrentRequest')
            ->willReturn(new Request());

        $productService = new ProductService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        $result = $productService->getProducts([
            'offset' => 0,
            'limit' => 10,
            'stores' => [1],
        ]);

        self::assertIsArray($result);
        self::assertArrayHasKey('_links', $result);
        self::assertArrayHasKey('offset', $result);
        self::assertArrayHasKey('limit', $result);
        self::assertArrayHasKey('size', $result);
        self::assertArrayHasKey('total', $result);
        self::assertArrayHasKey('results', $result);
    }

    public function testValidateParameters()
    {
        $productService = new ProductService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertCount(3, $productService->validateParameters(['latitude' => 'foo']));
        self::assertCount(2, $productService->validateParameters(['latitude' => '1']));
        self::assertCount(1, $productService->validateParameters(['latitude' => '1', 'longitude' => '1']));
        self::assertCount(3, $productService->validateParameters(['longitude' => 'foo']));
        self::assertCount(2, $productService->validateParameters(['longitude' => '1']));
        self::assertCount(3, $productService->validateParameters(['distance' => 'foo']));
        self::assertCount(1, $productService->validateParameters([
            'latitude' => '1',
            'longitude' => '1',
            'distance' => 'foo',
        ]));
        self::assertCount(0, $productService->validateParameters([
            'latitude' => '1',
            'longitude' => '1',
            'distance' => 100,
        ]));
        self::assertCount(1, $productService->validateParameters(['stores' => 'foo']));
        self::assertCount(1, $productService->validateParameters([
            'stores' => [
                'foo',
                1
            ]
        ]));
        self::assertCount(0, $productService->validateParameters([
            'stores' => [
                1,
                2
            ]
        ]));
        self::assertCount(1, $productService->validateParameters(['limit' => 'foo']));
        self::assertCount(1, $productService->validateParameters(['limit' => -1]));
        self::assertCount(0, $productService->validateParameters(['limit' => 5]));
        self::assertCount(1, $productService->validateParameters(['offset' => 'foo']));
        self::assertCount(1, $productService->validateParameters(['offset' => -5]));
        self::assertCount(0, $productService->validateParameters(['offset' => 5]));
    }
}