<?php

namespace App\Tests\Service;

use App\Service\StoreService;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Index;
use Elastica\ResultSet;
use JoliCode\Elastically\Client;
use JoliCode\Elastically\Result;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

class StoreServiceTest extends TypeTestCase
{
    private $requestStackMock;
    private $busMock;
    private $elkMock;
    private $emMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->requestStackMock = $this->getMockBuilder(RequestStack::class)->disableOriginalConstructor()->getMock();
        $this->formFactoryMock = $this->getMockBuilder(FormFactoryInterface::class)->disableOriginalConstructor()->getMock();
        $this->busMock = $this->getMockBuilder(MessageBusInterface::class)->disableOriginalConstructor()->getMock();
        $this->elkMock = $this->getMockBuilder(Client::class)->disableOriginalConstructor()->getMock();
        $this->emMock = $this->getMockBuilder(EntityManagerInterface::class)->disableOriginalConstructor()->disableArgumentCloning()->getMock();
    }

    public function testAddStoreFailed()
    {
        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('isValid')
            ->willReturn(false);

        $this->formFactoryMock->expects(self::any())
            ->method('create')
            ->willReturn($formMock);

        $storeService = new StoreService(
            $this->requestStackMock,
            $this->formFactoryMock,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertInstanceOf(
            Form::class,
            $storeService->addStore([])
        );
    }

    public function testAddStore()
    {
        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('isValid')
            ->willReturn(true);

        $this->formFactoryMock->expects(self::any())
            ->method('create')
            ->willReturn($formMock);

        $this->emMock->expects(self::any())
            ->method('flush')
            ->will($this->returnCallback(function($arg){
                $reflection = new \ReflectionClass($arg);
                $property = $reflection->getProperty('id');
                $property->setAccessible(true);
                $property->setValue($arg, 1);

                return $arg;
            }));

        $storeService = new StoreService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        self::assertSame(
            ['id' => 1],
            $storeService->addStore([
                'name' => 'foo',
            ])
        );
    }

    public function testGetStoresNoResults()
    {
        $storeService = new StoreService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        $result = $storeService->getStores([
            'offset' => 0,
            'limit' => 10,
        ]);

        self::assertInstanceOf(NotFoundHttpException::class, $result);
    }

    public function testGetStores()
    {
        $storeModel = new \App\Model\Store();
        $storeModel->id = 1;
        $storeModel->name = 'acme';
        $storeModel->geo = [
            'lat' => '1',
            'lon' => '2'
        ];
        $storeModel->manager = [
            'id' => 1,
            'first_name' => 'John',
            'last_name' => 'Doe',
        ];
        $result = new Result([
            'sort' => [
                1500,
            ]
        ]);
        $result->setModel($storeModel);

        $resultSet = $this->getMockBuilder(ResultSet::class)->disableOriginalConstructor()->getMock();
        $resultSet->expects(self::atLeastOnce())
            ->method('count')
            ->willReturn(1);
        $resultSet->expects(self::atLeastOnce())
            ->method('getTotalHits')
            ->willReturn(1);
        $resultSet->expects(self::atLeastOnce())
            ->method('getResults')
            ->willReturn([$result]);

        $indexMock = $this->getMockBuilder(Index::class)->disableOriginalConstructor()->getMock();
        $indexMock->expects(self::atLeastOnce())
            ->method('search')
            ->willReturn($resultSet);

        $this->elkMock->expects(self::atLeastOnce())
            ->method('getIndex')
            ->willReturn($indexMock);

        $this->requestStackMock->expects(self::atLeastOnce())
            ->method('getCurrentRequest')
            ->willReturn(new Request());

        $storeService = new StoreService(
            $this->requestStackMock,
            $this->factory,
            $this->emMock,
            $this->busMock,
            $this->elkMock
        );

        $result = $storeService->getStores([
            'offset' => 0,
            'limit' => 10,
            'name' => 'acme',
            'latitude' => 1,
            'longitude' => 2,
            'distance' => 1000,
        ]);

        self::assertIsArray($result);
        self::assertArrayHasKey('_links', $result);
        self::assertArrayHasKey('offset', $result);
        self::assertArrayHasKey('limit', $result);
        self::assertArrayHasKey('size', $result);
        self::assertArrayHasKey('total', $result);
        self::assertArrayHasKey('results', $result);
    }
}