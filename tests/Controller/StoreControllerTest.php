<?php

namespace App\Tests\Controller;

use App\Controller\ProductController;
use App\Controller\StoreController;
use App\Service\ProductService;
use App\Service\StoreService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationList;

class StoreControllerTest extends TestCase
{
    public function testAddStoreError()
    {
        $storeService = $this->getMockBuilder(StoreService::class)->disableOriginalConstructor()->getMock();

        $controller = new StoreController();

        self::assertInstanceOf(
            BadRequestException::class,
            $controller->addStore(new Request(), $storeService)
        );
    }

    public function testGetProductsError()
    {
        $constraints = $this->getMockBuilder(ConstraintViolationList::class)->disableOriginalConstructor()->getMock();
        $constraints->expects(self::atLeastOnce())
            ->method('count')
            ->willReturn(2);
        $storeService = $this->getMockBuilder(StoreService::class)->disableOriginalConstructor()->getMock();
        $storeService->expects(self::atLeastOnce())
            ->method('validateParameters')
            ->willReturn($constraints);

        $controller = new StoreController();

        self::assertInstanceOf(
            ConstraintViolationList::class,
            $controller->getStores(new Request(), $storeService)
        );
    }
}