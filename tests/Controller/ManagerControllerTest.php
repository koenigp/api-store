<?php

namespace App\Tests\Controller;

use App\Controller\ManagerController;
use App\Service\ManagerService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;

class ManagerControllerTest extends TestCase
{
    public function testAddManagerError()
    {
        $managerService = $this->getMockBuilder(ManagerService::class)->disableOriginalConstructor()->getMock();

        $controller = new ManagerController();

        self::assertInstanceOf(
            BadRequestException::class,
            $controller->addManager(new Request(), $managerService)
        );

    }

}