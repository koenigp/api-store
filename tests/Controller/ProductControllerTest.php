<?php

namespace App\Tests\Controller;

use App\Controller\ProductController;
use App\Service\ProductService;
use FOS\HttpCacheBundle\Http\SymfonyResponseTagger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class ProductControllerTest extends TestCase
{
    public function testAddProductError()
    {
        $productService = $this->getMockBuilder(ProductService::class)->disableOriginalConstructor()->getMock();

        $controller = new ProductController();

        self::assertInstanceOf(
            BadRequestException::class,
            $controller->addProduct(new Request(), $productService)
        );
    }

    public function testGetProductsError()
    {
        $constraints = $this->getMockBuilder(ConstraintViolationList::class)->disableOriginalConstructor()->getMock();
        $constraints->expects(self::atLeastOnce())
            ->method('count')
            ->willReturn(2);
        $productService = $this->getMockBuilder(ProductService::class)->disableOriginalConstructor()->getMock();
        $productService->expects(self::atLeastOnce())
            ->method('validateParameters')
            ->willReturn($constraints);

        $responseTagger = $this->getMockBuilder(SymfonyResponseTagger::class)->disableOriginalConstructor()->getMock();

        $controller = new ProductController();

        self::assertInstanceOf(
            ConstraintViolationList::class,
            $controller->getProducts(new Request(), $productService, $responseTagger)
        );
    }
}