<?php

namespace App\Tests\EventSubscriber;

use App\EventSubscriber\ViewSubscriber;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class ViwSubscriberTest extends TestCase
{
    public function testEventSubscription()
    {
        $this->assertArrayHasKey(KernelEvents::VIEW, ViewSubscriber::getSubscribedEvents());
    }

    public function testRenderViewSuccess()
    {
        $subscriber = new ViewSubscriber();
        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();
        $event = new ViewEvent($kernel, new Request(), 1, ['test']);

        $subscriber->renderView($event);
        $this->assertInstanceOf(JsonResponse::class, $event->getResponse());
        $this->assertSame('["test"]', $event->getResponse()->getContent());
        $this->assertSame(200, $event->getResponse()->getStatusCode());
    }

    public function testRenderValidatorErrors()
    {
        $subscriber = new ViewSubscriber();
        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();

        $constraints = new ConstraintViolationList(
            [
                new ConstraintViolation('error1', '', [], '', 'path1', ''),
                new ConstraintViolation('error2', '', [], '', 'path2', ''),
            ]
        );

        $event = new ViewEvent($kernel, new Request(), 1, $constraints);

        $subscriber->renderView($event);
        $this->assertInstanceOf(JsonResponse::class, $event->getResponse());
        $this->assertSame(
            '{"message":"'.ViewSubscriber::BAD_REQUEST_MESSSAGE.'","errors":{"path1":["error1"],"path2":["error2"]}}',
            $event->getResponse()->getContent()
        );
        $this->assertSame(400, $event->getResponse()->getStatusCode());
    }

    public function testRenderViewFormErrors()
    {
        $subscriber = new ViewSubscriber();
        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();

        $childFormMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $childFormMock->expects(self::any())
            ->method('isValid')
            ->willReturn(false);
        $childFormMock->expects(self::any())
            ->method('getErrors')
            ->willReturn([new FormError('error2')]);
        $childFormMock->expects(self::any())
            ->method('getName')
            ->willReturn('path');
        $childFormMock->expects(self::any())
            ->method('all')
            ->willReturn([]);

        $formMock = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $formMock->expects(self::any())
            ->method('getErrors')
            ->willReturn([new FormError('error1')]);
        $formMock->expects(self::any())
            ->method('isRoot')
            ->willReturn(true);
        $formMock->expects(self::any())
            ->method('all')
            ->willReturn([$childFormMock]);

        $event = new ViewEvent($kernel, new Request(), 1, $formMock);

        $subscriber->renderView($event);
        $this->assertInstanceOf(JsonResponse::class, $event->getResponse());
        $this->assertSame(
            '{"message":"'.ViewSubscriber::BAD_REQUEST_MESSSAGE.'","errors":{"global":["error1"],"path":["error2"]}}',
            $event->getResponse()->getContent()
        );
        $this->assertSame(400, $event->getResponse()->getStatusCode());
    }

    public function testRenderViewFormErrorsBadRequest()
    {
        $subscriber = new ViewSubscriber();
        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();
        $event = new ViewEvent($kernel, new Request(), 1, new class{});

        $subscriber->renderView($event);
        $this->assertInstanceOf(JsonResponse::class, $event->getResponse());
        $this->assertSame(
            '{"message":"'.ViewSubscriber::BAD_REQUEST_MESSSAGE.'"}',
            $event->getResponse()->getContent()
        );
        $this->assertSame(400, $event->getResponse()->getStatusCode());
    }

    public function testRenderViewFormNoResults()
    {
        $subscriber = new ViewSubscriber();
        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();
        $event = new ViewEvent($kernel, new Request(), 1, new NotFoundHttpException());

        $subscriber->renderView($event);
        $this->assertInstanceOf(JsonResponse::class, $event->getResponse());
        $this->assertSame(
            '{"message":"No Search Results Found"}',
            $event->getResponse()->getContent()
        );
        $this->assertSame(404, $event->getResponse()->getStatusCode());
    }

    public function testRenderViewBadRequestException()
    {
        $subscriber = new ViewSubscriber();
        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();
        $event = new ViewEvent($kernel, new Request(), 1, new BadRequestException());

        $subscriber->renderView($event);
        $this->assertInstanceOf(JsonResponse::class, $event->getResponse());
        $this->assertSame(
            '{"message":"'.ViewSubscriber::BAD_REQUEST_MESSSAGE.'"}',
            $event->getResponse()->getContent()
        );
        $this->assertSame(400, $event->getResponse()->getStatusCode());
    }

}