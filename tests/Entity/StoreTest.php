<?php

namespace App\Tests\Entity;

use App\Entity\LinkProductStore;
use App\Entity\Manager;
use App\Entity\Store;
use PHPUnit\Framework\TestCase;

class StoreTest extends TestCase
{
    /**
     * data provider for set get.
     */
    public function dataSetGet()
    {
        yield [
            'name', 'name'.rand(10, 100),
        ];
        yield [
            'address', 'address'.rand(10, 100),
        ];
        yield [
            'latitude', mt_rand() / mt_getrandmax(),
        ];
        yield [
            'longitude', mt_rand() / mt_getrandmax(),
        ];
        yield [
            'manager', new Manager(),
        ];
    }

    /**
     * @param $field
     * @param $expectedValue
     *
     * @dataProvider dataSetGet
     */
    public function testGetSet($field, $expectedValue)
    {
        $store = new Store();

        $set = 'set'.ucfirst($field);
        $get = 'get'.ucfirst($field);

        $store->$set($expectedValue);

        self::assertSame($expectedValue, $store->$get());
    }

    public function testLinkProductStore()
    {
        $linkProductStore = new LinkProductStore();

        $store = new Store();
        $store->addLinkProductStore($linkProductStore);

        self::assertSame($linkProductStore, $store->getLinkProductStores()->first());

        $store->removeLinkProductStore($linkProductStore);

        self::assertEmpty($store->getLinkProductStores());
    }

    public function testToModel()
    {
        $manager = new Manager();
        $manager->setFirstName('John')
            ->setLastName('Doe');
        $reflection = new \ReflectionClass($manager);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($manager, 1);

        $store = new Store();
        $store->setName('acme')
            ->setAddress('address')
            ->setLatitude(1)
            ->setLongitude(1)
            ->setManager($manager);
        $reflection = new \ReflectionClass($store);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($store, 1);

        $storeModel = $store->toModel();
        self::assertSame($store->getLatitude(), $storeModel->geo->lat);
        self::assertSame($store->getLongitude(), $storeModel->geo->lon);
        self::assertSame($store->getName(), $storeModel->name);
        self::assertSame($store->getAddress(), $storeModel->address);
        self::assertSame($store->getId(), $storeModel->id);
        self::assertSame($store->getManager()->getId(), $storeModel->manager->id);
        self::assertSame($store->getManager()->getLastName(), $storeModel->manager->last_name);
        self::assertSame($store->getManager()->getFirstName(), $storeModel->manager->first_name);
    }
}