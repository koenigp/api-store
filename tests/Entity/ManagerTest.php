<?php

namespace App\Tests\Entity;

use App\Entity\Manager;
use App\Entity\Store;
use PHPUnit\Framework\TestCase;

class ManagerTest extends TestCase
{
    /**
     * data provider for set get.
     */
    public function dataSetGet()
    {
        yield [
            'firstName', 'firstName'.rand(10, 100),
        ];
        yield [
            'lastName', 'lastName'.rand(10, 100),
        ];
    }

    /**
     * @param $field
     * @param $expectedValue
     *
     * @dataProvider dataSetGet
     */
    public function testGetSet($field, $expectedValue)
    {
        $manager = new Manager();

        $set = 'set'.ucfirst($field);
        $get = 'get'.ucfirst($field);

        $manager->$set($expectedValue);

        self::assertSame($expectedValue, $manager->$get());
    }

    public function testStores()
    {
        $manager = new Manager();

        $store = new Store();
        $store->setName('acme');
        $store->setManager($manager);

        $manager->addStore($store);

        self::assertSame($store, $manager->getStores()->first());

        $manager->removeStore($store);

        self::assertEmpty($manager->getStores());
    }
}