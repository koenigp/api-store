<?php

namespace App\Tests\Entity;

use App\Entity\LinkProductStore;
use App\Entity\Manager;
use App\Entity\Product;
use App\Entity\Store;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{


    /**
     * data provider for set get.
     */
    public function dataSetGet()
    {
        yield [
            'name', 'name'.rand(10, 100),
        ];
        yield [
            'photo', 'photo'.rand(10, 100),
        ];
    }

    /**
     * @param $field
     * @param $expectedValue
     *
     * @dataProvider dataSetGet
     */
    public function testGetSet($field, $expectedValue)
    {
        $store = new Product();

        $set = 'set'.ucfirst($field);
        $get = 'get'.ucfirst($field);

        $store->$set($expectedValue);

        self::assertSame($expectedValue, $store->$get());
    }

    public function testLinkProductStore()
    {
        $linkProductStore = new LinkProductStore();

        $product = new Product();
        $product->addLinkProductStore($linkProductStore);

        self::assertSame($linkProductStore, $product->getLinkProductStores()->first());

        $product->removeLinkProductStore($linkProductStore);

        self::assertEmpty($product->getLinkProductStores());
    }

    public function testToModel()
    {
        $store = new Store();
        $store->setName('acme');
        $reflection = new \ReflectionClass($store);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($store, 1);

        $linkProductStore = new LinkProductStore();
        $linkProductStore->setStock(45)
            ->setStore($store);

        $product = new Product();
        $product->setName('Acme')
            ->setPhoto('photo')
            ->addLinkProductStore($linkProductStore);
        $reflection = new \ReflectionClass($product);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($product, 1);

        $productModel = $product->toModel();
        self::assertSame($product->getId(), $productModel->id);
        self::assertSame($product->getName(), $productModel->name);
        self::assertSame($product->getPhoto(), $productModel->photo);
        self::assertSame($linkProductStore->getStore()->getId(), $productModel->stores[0]->id);
        self::assertSame($linkProductStore->getStore()->getName(), $productModel->stores[0]->name);
        self::assertSame($linkProductStore->getStock(), $productModel->stores[0]->stock);
        self::assertSame((int) $linkProductStore->getStock(), $productModel->stock_total);
    }
}