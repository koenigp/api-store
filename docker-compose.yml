version: "3.4"

services:
  app:
    build: docker/app
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.store.rule=Host(`api-store.localhost`)"
      - "traefik.http.routers.store.entrypoints=web"
    volumes:
      - .:/var/www
      - /var/www/var/cache
      - /var/www/var/log
      - ./docker/app/sites-enabled:/etc/apache2/sites-enabled
      - ./docker/app/entrypoint.sh:/opt/webops/docker/entrypoint.sh
    links:
      - db:db
      - redis:redis

  db:
    image: mysql:8
    volumes:
      - ./docker/db/mysql:/var/lib/mysql
    ports:
      - "3306:3306"
    environment:
      - MYSQL_ROOT_PASSWORD=root

  myadmin:
    image: phpmyadmin/phpmyadmin
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.pma.rule=Host(`phpmyadmin.localhost`)"
      - "traefik.http.routers.pma.entrypoints=web"
    links:
      - db:db

  traefik:
    image: traefik:v2.0
    container_name: traefik
    command:
      - "--log.level=DEBUG"
      - "--api.insecure=true"
      - "--api.dashboard=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
    ports:
      - "80:80"
      - "8080:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

  elasticsearch:
    image: elasticsearch:7.13.0
    environment:
      - cluster.name=docker-cluster
      - bootstrap.memory_lock=true
      - discovery.type=single-node
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m" # 512mo HEAP
    ulimits:
      memlock:
        soft: -1
        hard: -1
    ports:
      - 9200:9200

  kibana:
    image: kibana:7.13.0
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.kibana.rule=Host(`kibana.localhost`)"
      - "traefik.http.routers.kibana.entrypoints=web"
      - "traefik.http.services.kibana.loadbalancer.server.port=5601"
    environment:
      ELASTICSEARCH_URL: http://elasticsearch:9200
    depends_on:
      - elasticsearch

  redis:
    image: redis:alpine
    volumes:
      - ./docker/redis/redis-data:/var/lib/redis
      - ./docker/redis/redis.conf:/usr/local/etc/redis/redis.conf
    ports:
      - "6379:6379"

  swagger:
    image: swaggerapi/swagger-ui
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.swagger.rule=Host(`swagger.localhost`)"
      - "traefik.http.routers.swagger.entrypoints=web"
      - "traefik.http.services.swagger.loadbalancer.server.port=8080"
    volumes:
      - ./openapi.json:/openapi.json
    environment:
      SWAGGER_JSON: /openapi.json