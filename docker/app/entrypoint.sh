#!/bin/sh

mkdir -p /var/www/var/cache
mkdir -p /var/www/var/log
setfacl -Rm u:www-data:rwX /var/www/var/cache /var/www/var/log

set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
        set -- apache2-foreground "$@"
fi

exec "$@"