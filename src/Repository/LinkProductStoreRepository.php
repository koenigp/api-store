<?php

namespace App\Repository;

use App\Entity\LinkProductStore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LinkProductStore|null find($id, $lockMode = null, $lockVersion = null)
 * @method LinkProductStore|null findOneBy(array $criteria, array $orderBy = null)
 * @method LinkProductStore[]    findAll()
 * @method LinkProductStore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkProductStoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LinkProductStore::class);
    }

    // /**
    //  * @return LinkProductStore[] Returns an array of LinkProductStore objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LinkProductStore
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
