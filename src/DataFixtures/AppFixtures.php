<?php

namespace App\DataFixtures;

use App\Entity\LinkProductStore;
use App\Entity\Manager;
use App\Entity\Product;
use App\Entity\Store;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /** @var Generator $faker */
        $faker = Factory::create('fr_FR');

        $managers =[];
        for ($i=0; $i<5; $i++) {
            $storeManager = new Manager();
            $storeManager->setFirstName($faker->firstName)
                ->setLastName($faker->lastName);
            $manager->persist($storeManager);
            $managers[$i] = $storeManager;
        }

        $stores = [];
        for ($i=0; $i<10; $i++) {
            $store = new Store();
            $store->setName($faker->unique()->company)
                ->setAddress($faker->address)
                ->setLatitude($faker->latitude)
                ->setLongitude($faker->longitude)
                ->setManager($managers[$faker->numberBetween(0,4)]);
            $manager->persist($store);
            $stores[$i] = $store;
        }

        $products = [];
        for ($i=0; $i<30; $i++) {
            $product = new Product();
            $product->setName('item_'.$i+1)
                ->setPhoto('https://media.sezane.com/image/upload/' . $faker->word . '.png');
            $manager->persist($product);
            $products[$i] = $product;
        }

        foreach ($stores as $store) {
            for ($i=0; $i<15; $i++) {
                $stock = new LinkProductStore();
                $stock->setStore($store)
                    ->setProduct($products[$faker->unique()->numberBetween(0,29)])
                    ->setStock($faker->numberBetween(0,1000));
                $manager->persist($stock);
            }
            $faker->unique(true);
        }

        $manager->flush();
    }
}
