<?php

namespace App\Entity;

use App\Repository\LinkProductStoreRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LinkProductStoreRepository::class)
 * @ORM\Table(uniqueConstraints={
 *      @ORM\UniqueConstraint(name="product_store", columns={"product_id", "store_id"})
 * })
 * @UniqueEntity(
 *     fields={"store", "product"}
 * )
 */
class LinkProductStore
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Store::class, inversedBy="linkProductStores")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private Store|null $store;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="linkProductStores")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private Product|null $product;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private int $stock;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getStock(): ?string
    {
        return $this->stock;
    }

    public function setStock(string $stock): self
    {
        $this->stock = $stock;

        return $this;
    }
}
