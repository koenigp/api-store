<?php

namespace App\Entity;

use App\Model\GeoPoint;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @UniqueEntity("name")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Assert\NotBlank
     * @Assert\Length (max = 1024)
     */
    private string $photo;

    /**
     * @ORM\OneToMany(targetEntity=LinkProductStore::class, mappedBy="product", cascade={"persist"})
     */
    private Collection $linkProductStores;

    public function __construct()
    {
        $this->linkProductStores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection|LinkProductStore[]
     */
    public function getLinkProductStores(): Collection
    {
        return $this->linkProductStores;
    }

    public function addLinkProductStore(LinkProductStore $linkProductStore): self
    {
        if (!$this->linkProductStores->contains($linkProductStore)) {
            $linkProductStore->setProduct($this);
            $this->linkProductStores[] = $linkProductStore;
        }

        return $this;
    }

    public function removeLinkProductStore(LinkProductStore $linkProductStore): self
    {
        if ($this->linkProductStores->removeElement($linkProductStore)) {
            // set the owning side to null (unless already changed)
            if ($linkProductStore->getProduct() === $this) {
                $linkProductStore->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * Product DTO
     * @return \App\Model\Product
     */
    public function toModel(): \App\Model\Product
    {
        $model = new \App\Model\Product();
        $model->id = $this->id;
        $model->name = $this->name;
        $model->photo = $this->photo;

        $stock = 0;
        foreach ($this->getLinkProductStores() as $linkProductStore) {
            $store = new \App\Model\ProductStore();

            $store->id = $linkProductStore->getStore()->getId();
            $store->name = $linkProductStore->getStore()->getName();
            $store->stock = $linkProductStore->getStock();

            $stock += $linkProductStore->getStock();

            $model->stores[] = $store;
        }
        $model->stock_total = $stock;

        return $model;
    }
}
