<?php

namespace App\Entity;

use App\Model\GeoPoint;
use App\Repository\StoreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=StoreRepository::class)
 * @UniqueEntity("name")
 */
class Store
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private string $address;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=16)
     * @Assert\Regex("/^(\-?([0-8]?[0-9](\.\d+)?|90(.[0]+)?)?)$/")
     */
    private float $latitude;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=16)
     * @Assert\Regex("/^(\-?([1]?[0-7]?[0-9](\.\d+)?|180((.[0]+)?)))$/")
     */
    private float $longitude;

    /**
     * @ORM\ManyToOne(targetEntity=Manager::class, inversedBy="stores")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private Manager|null $manager;

    /**
     * @ORM\OneToMany(targetEntity=LinkProductStore::class, mappedBy="store")
     */
    private Collection|null $linkProductStores;

    public function __construct()
    {
        $this->linkProductStores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getManager(): ?Manager
    {
        return $this->manager;
    }

    public function setManager(?Manager $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return Collection|LinkProductStore[]
     */
    public function getLinkProductStores(): Collection
    {
        return $this->linkProductStores;
    }

    public function addLinkProductStore(LinkProductStore $linkProductStore): self
    {
        if (!$this->linkProductStores->contains($linkProductStore)) {
            $this->linkProductStores[] = $linkProductStore;
            $linkProductStore->setStore($this);
        }

        return $this;
    }

    public function removeLinkProductStore(LinkProductStore $linkProductStore): self
    {
        if ($this->linkProductStores->removeElement($linkProductStore)) {
            // set the owning side to null (unless already changed)
            if ($linkProductStore->getStore() === $this) {
                $linkProductStore->setStore(null);
            }
        }

        return $this;
    }

    /**
     * Store DTO
     * @return \App\Model\Store
     */
    public function toModel(): \App\Model\Store
    {
        $manager = new \App\Model\Manager();
        $manager->id = $this->getManager()->getId();
        $manager->first_name = $this->getManager()->getFirstName();
        $manager->last_name = $this->getManager()->getLastName();

        $geo = new GeoPoint();
        $geo->lat = $this->latitude;
        $geo->lon = $this->longitude;

        $model = new \App\Model\Store();
        $model->id = $this->id;
        $model->name = $this->name;
        $model->address = $this->address;
        $model->manager = $manager;
        $model->geo = $geo;

        return $model;
    }
}
