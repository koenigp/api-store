<?php

namespace App\Elasticsearch;

use App\Model\Product;
use App\Model\Store;
use App\Repository\ProductRepository;
use App\Repository\StoreRepository;
use Elastica\Document;
use JoliCode\Elastically\Messenger\DocumentExchangerInterface;

class DocumentExchanger implements DocumentExchangerInterface
{
    private ProductRepository $productRepository;

    private StoreRepository $storeRepository;

    /**
     * DocumentExchanger constructor.
     * @param ProductRepository $productRepository
     * @param StoreRepository $storeRepository
     */
    public function __construct(ProductRepository $productRepository, StoreRepository $storeRepository)
    {
        $this->productRepository = $productRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @param string $className
     * @param string $id
     * @return Document|null
     */
    public function fetchDocument(string $className, string $id): ?Document
    {
        if ($className === Product::class) {
            $product = $this->productRepository->find($id);

            if ($product) {
                return new Document($id, $product->toModel());
            }
        }

        if ($className === Store::class) {
            $store = $this->storeRepository->find($id);

            if ($store) {
                return new Document($id, $store->toModel());
            }
        }

        return null;
    }
}