<?php

namespace App\Elasticsearch\Command;

use App\Repository\ProductRepository;
use App\Repository\StoreRepository;
use Elastica\Document;
use JoliCode\Elastically\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateIndexCommand extends Command
{
    protected static $defaultName = 'app:elasticsearch:create-index';
    private Client $client;
    private ProductRepository $productRepository;
    private StoreRepository $storeRepository;

    protected function configure()
    {
        $this
            ->setDescription('Build new product index from scratch and populate.')
        ;
    }

    public function __construct(
        Client $client,
        ProductRepository $productRepository,
        StoreRepository $storeRepository
    ){
        parent::__construct();
        $this->client = $client;
        $this->productRepository = $productRepository;
        $this->storeRepository = $storeRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $allStores = $this->storeRepository->createQueryBuilder('store')->getQuery()->getResult();
        $this->createIndex('store', $allStores);

        $allProducts = $this->productRepository->createQueryBuilder('product')->getQuery()->getResult();
        $this->createIndex('product', $allProducts);

        return Command::SUCCESS;
    }

    private function createIndex(string $index, array $items)
    {
        $indexBuilder = $this->client->getIndexBuilder();
        $newIndex = $indexBuilder->createIndex($index);
        $indexer = $this->client->getIndexer();

        foreach ($items as $item) {
            $indexer->scheduleIndex($newIndex, new Document($item->getId(), $item->toModel()));
        }

        $indexer->flush();

        $indexBuilder->markAsLive($newIndex, $index);
        $indexBuilder->speedUpRefresh($newIndex);
        $indexBuilder->purgeOldIndices($index);
    }
}