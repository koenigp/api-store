<?php

namespace App\Controller;

use App\Service\ManagerService;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ManagerController
 */
#[Route('/api/managers')]
class ManagerController extends AbstractController
{
    /**
     * @param Request $request
     * @param ManagerService $managerService
     * @return Array|\Exception
     *
     * @OA\Post(
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *             @OA\Property(property="firstName", type="string"),
     *             @OA\Property(property="lastName", type="string")
     *          )
     *      )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *          @OA\Property(property="id", type="integer")
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="No results",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string")
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Bad request",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string"),
     *          @OA\Property(
     *              property="errors",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="error_field", type="string")
     *              )
     *          )
     *     )
     * )
     */
    #[Route('', name: 'managers_create', methods: 'POST')]
    public function addManager(Request $request, ManagerService $managerService): Array|BadRequestException
    {
        $content = json_decode($request->getContent(), true);

        if (empty($content)) {
            return new BadRequestException();
        }

        return $managerService->addManager($content);
    }
}
