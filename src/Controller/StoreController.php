<?php

namespace App\Controller;

use App\Service\StoreService;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class StoreController
 */
#[Route('/api/stores')]
class StoreController extends AbstractController
{
    /**
     * @param Request $request
     * @param StoreService $storeService
     * @return Array|\Exception|Form
     *
     * @OA\Post(
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="address", type="string"),
     *             @OA\Property(property="latitude", type="string"),
     *             @OA\Property(property="longitude", type="string"),
     *             @OA\Property(property="manager", type="integer", description="Manager id")
     *          )
     *      )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *          @OA\Property(property="id", type="integer")
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="No results",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string")
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Bad request",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string"),
     *          @OA\Property(
     *              property="errors",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="error_field", type="string")
     *              )
     *          )
     *     )
     * )
     */
    #[Route('', name: 'stores_create', methods: 'POST')]
    public function addStore(Request $request, StoreService $storeService): Array|BadRequestException|Form
    {
        $content = json_decode($request->getContent(), true);

        if (empty($content)) {
            return new BadRequestException();
        }

        return $storeService->addStore($content);
    }

    /**
     * @param Request $request
     * @param StoreService $storeService
     * @return array|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException|\Symfony\Component\Validator\ConstraintViolationList
     *
     * @OA\Response(
     *     response=200,
     *     description="Get products success",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="_links",
     *          type="object",
     *          @OA\Property(property="self", type="string"),
     *          @OA\Property(property="next", type="string"),
     *          @OA\Property(property="prev", type="string")
     *        ),
     *        @OA\Property(property="offset", type="integer"),
     *        @OA\Property(property="limit", type="integer"),
     *        @OA\Property(property="size", type="integer"),
     *        @OA\Property(property="total", type="integer"),
     *        @OA\Property(
     *          property="results",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(property="id", type="integer"),
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="address", type="string"),
     *              @OA\Property(
     *                  property="geo",
     *                  type="object",
     *                  @OA\Property(property="lat", type="number"),
     *                  @OA\Property(property="lon", type="number")
     *              ),
     *              @OA\Property(
     *                  property="manager",
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="first_name", type="string"),
     *                  @OA\Property(property="last_name", type="string")
     *              )
     *           )
     *        ),
     *        @OA\Property(
     *          property="distance",
     *          type="integer",
     *          description="distance from store, present only if filtered by distance"
     *        )
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="No results",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string")
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Bad request",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string"),
     *          @OA\Property(
     *              property="errors",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="error_field", type="string")
     *              )
     *          )
     *     )
     * )
     *
     * @OA\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Return products limit",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default=0
     *     )
     * )
     * @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Pagination start",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default=10
     *     )
     * )
     * @OA\Parameter(
     *     name="latitude",
     *     in="query",
     *     description="Latitude",
     *     required=false,
     *     @OA\Schema(type="number")
     * )
     * @OA\Parameter(
     *     name="longitude",
     *     in="query",
     *     description="Longitude",
     *     required=false,
     *     @OA\Schema(type="number")
     * )
     * @OA\Parameter(
     *     name="distance",
     *     in="query",
     *     description="Distance in meters",
     *     required=false,
     *     @OA\Schema(type="integer")
     * )
     */
    #[Route('', name: 'stores_get', methods: 'GET')]
    public function getStores(Request $request, StoreService $storeService): Array|ConstraintViolationList|NotFoundHttpException
    {
        $parameters = [
            'offset' => $request->get('offset', 0),
            'limit'  => $request->get('limit', 10),
            'name'  => $request->get('name'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'distance' => $request->get('distance'),
        ];

        $violations = $storeService->validateParameters($parameters);
        if (count($violations) > 0) {
            return $violations;
        }

        return $storeService->getStores($parameters);
    }
}
