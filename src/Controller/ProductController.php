<?php

namespace App\Controller;

use App\Service\ProductService;
use FOS\HttpCacheBundle\Http\SymfonyResponseTagger;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationList;


/**
 * Class ProductController
 */
#[Route('/api/products')]
class ProductController extends AbstractController
{
    /**
     * @param Request $request
     * @param ProductService $productService
     * @return Array|BadRequestException|Form
     *
     * @OA\Post(
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="photo", type="string"),
     *             @OA\Property(
     *               property="stores",
     *               type="array",
     *               @OA\Items(
     *                   @OA\Property(property="store", type="integer", description="store id"),
     *                   @OA\Property(property="stock", type="integer")
     *                )
     *             )
     *          )
     *      )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *          @OA\Property(property="id", type="integer")
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="No results",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string")
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Bad request",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string"),
     *          @OA\Property(
     *              property="errors",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="error_field", type="string")
     *              )
     *          )
     *     )
     * )
     */
    #[Route('', name: 'products_create', methods: 'POST')]
    public function addProduct(Request $request, ProductService $productService): Array|BadRequestException|Form
    {
        $content = json_decode($request->getContent(), true);

        if (empty($content)) {
            return new BadRequestException();
        }
        return $productService->addProduct($content);
    }


    /**
     * @param Request $request
     * @param ProductService $productService
     * @return array|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException|\Symfony\Component\Validator\ConstraintViolationList
     *
     *
     * @OA\Response(
     *     response=200,
     *     description="Get products success",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="_links",
     *          type="object",
     *          @OA\Property(property="self", type="string"),
     *          @OA\Property(property="next", type="string"),
     *          @OA\Property(property="prev", type="string")
     *        ),
     *        @OA\Property(property="offset", type="integer"),
     *        @OA\Property(property="limit", type="integer"),
     *        @OA\Property(property="size", type="integer"),
     *        @OA\Property(property="total", type="integer"),
     *        @OA\Property(
     *          property="results",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(property="id", type="integer"),
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="photo", type="string"),
     *              @OA\Property(
     *                  property="stores",
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(property="photo", type="string")
     *              )
     *           )
     *        )
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="No results",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string")
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Bad request",
     *     @OA\JsonContent(
     *          @OA\Property(property="message", type="string"),
     *          @OA\Property(
     *              property="errors",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="error_field", type="string")
     *              )
     *          )
     *     )
     * )
     *
     * @OA\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Return products limit",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default=0
     *     )
     * )
     * @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Pagination start",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default=10
     *     )
     * )
     * @OA\Parameter(
     *     name="stores[]",
     *     in="query",
     *     description="Stores id",
     *     required=false,
     *     @OA\Schema(
     *          type="array",
     *          @OA\Items( type="integer")
     *     )
     * )
     */
    #[Route('', name: 'products_get', methods: 'GET')]
    public function getProducts(
        Request $request,
        ProductService $productService,
        SymfonyResponseTagger $responseTagger
    ): Array|ConstraintViolationList|NotFoundHttpException
    {
        $parameters = [
            'offset' => $request->get('offset', 0),
            'limit'  => $request->get('limit', 10),
            'stores' => $request->get('stores'),
        ];

        $violations = $productService->validateParameters($parameters);
        if (count($violations) > 0) {
            return $violations;
        }

        if (!empty($parameters['stores'])) {
            $storesId = $parameters['stores'];
            array_walk($storesId, function(&$storeId) {
                $storeId = 'store_'.$storeId;
            });
            $responseTagger->addTags($storesId);
        }

        return $productService->getProducts($parameters);
    }
}
