<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolationList;


class ViewSubscriber implements EventSubscriberInterface
{
    CONST BAD_REQUEST_MESSSAGE = 'Bad Request';

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::VIEW =>
                ['renderView', 10],
        ];
    }

    /**
     * Handle the output from the controllers.
     *
     * @param ViewEvent  $event
     */
    public function renderView($event)
    {
        $result = $event->getControllerResult();

        switch (true) {
            case $result instanceof NotFoundHttpException:
                $result = ['message' => 'No Search Results Found'];
                $httpCode = '404';
                break;
            case is_array($result):
                $httpCode = '200';
                break;
            case $result instanceof BadRequestException:
                $result = ['message' => self::BAD_REQUEST_MESSSAGE];
                $httpCode = '400';
                break;
            case $result instanceof Form:
                $result = [
                    'message' => self::BAD_REQUEST_MESSSAGE,
                    'errors'  => $this->getFormErrors($result),
                ];
                $httpCode = '400';
                break;
            case $result instanceof ConstraintViolationList:
                $result = [
                    'message' => self::BAD_REQUEST_MESSSAGE,
                    'errors' => $this->getValidatorErrors($result),
                ];
                $httpCode = '400';
                break;
            default:
                $result = ['message' => self::BAD_REQUEST_MESSSAGE];
                $httpCode = '400';
        }

        $response = new JsonResponse($result, $httpCode);

        $event->setResponse($response);
    }

    /**
     * @param Form $form
     * @return array
     */
    private function getFormErrors(Form $form): array
    {
        $errors = array();

        foreach ($form->getErrors() as $error) {
            if ($form->isRoot()) {
                $errors['global'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }

    /**
     * @param $errors
     * @return array
     */
    private function getValidatorErrors(ConstraintViolationList $errors): array
    {
        $list = [];

        foreach ($errors as $error) {
            $list[$error->getPropertyPath()][] = $error->getMessage();
        }

        return $list;
    }
}
