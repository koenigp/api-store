<?php

namespace App\Model;

class Store
{
    public $id;
    public $name;
    public $address;
    public $geo;
    public $manager;
}