<?php

namespace App\Model;

class Product
{
    public $id;
    public $name;
    public $photo;
    public $stock_total;

    /** @var array<ProductStore>|null  */
    public $stores;
}