<?php

namespace App\Service;

use App\Entity\Product;
use App\Form\ProductType;
use Elastica\Query;
use Elastica\ResultSet;
use JoliCode\Elastically\Messenger\IndexationRequest;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductService extends AbstractService
{
    /**
     * @param array $productData
     * @return Array|ProductType
     */
    public function addProduct(array $productData): Array|Form
    {
        $product = new Product();
        $productForm = $this->formFactory->create(ProductType::class, $product);
        $productForm->submit($productData);

        if (!$productForm->isValid()) {
            return $productForm;
        }

        // Persist product in linkProductStore
        foreach ($product->getLinkProductStores() as $linkProductStore) {
            $linkProductStore->setProduct($product);
        }

        $this->em->persist($product);
        $this->em->flush($product);

        $id = $product->getId();

        $this->bus->dispatch(new IndexationRequest(\App\Model\Product::class, $id));

        return [
            'id' => $id,
        ];
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function getProducts(array $parameters): array|NotFoundHttpException
    {
        $es_nested = null;

        // return stores only when filtered by store
        if (!empty($parameters['stores'])) {
            $es_terms = new Query\Terms('stores.id', $parameters['stores']);
            $es_bool = new Query\BoolQuery();
            $es_bool->addShould($es_terms);
            $innerHits = new Query\InnerHits();
            $innerHits->setSource(['stores']);
            $es_nested = new Query\Nested();
            $es_nested->setPath('stores')
                ->setQuery($es_bool)
                ->setInnerHits($innerHits);
        }

        $query = new Query($es_nested);

        // set source
        $query->setSource([
            'id',
            'name',
            'photo',
            'stock_total',
        ]);

        // Set offset and limit
        $query->setFrom($parameters['offset']);
        $query->setSize($parameters['limit']);

        /** @var ResultSet $foundProducts */
        $foundProducts = $this->elk->getIndex('product')->search($query);
        // Count of documents returned by elk
        $count = $foundProducts->count();
        if(0 === $count) {
            return new NotFoundHttpException();
        }

        $results = [];
        foreach ($foundProducts->getResults() as $result) {
            /** @var \App\Model\Product $foundProduct */
            $foundProduct = $result->getModel();

            $product = [
                'id' => $foundProduct->id,
                'name' => htmlspecialchars($foundProduct->name, ENT_COMPAT | ENT_HTML5),
                'photo' => htmlspecialchars($foundProduct->photo, ENT_COMPAT | ENT_HTML5),
                'stock_total' => $foundProduct->stock_total,
            ];

            $innerHits = $result->getInnerHits();

            if (!empty($innerHits) && !empty($innerHits['stores']['hits']['hits'])) {
                foreach ($innerHits['stores']['hits']['hits'] as $hit) {
                    $store = [
                        'id' => $hit['_source']['id'],
                        'name' => htmlspecialchars($hit['_source']['name'], ENT_COMPAT | ENT_HTML5),
                        'stock' => $hit['_source']['stock'],
                    ];
                    $product['stores'] = $store;
                }
            }

            $results[] = $product;
        }

        return [
            '_links' => $this->getLinks($count, $parameters),
            'offset' => (int) $parameters['offset'],
            'limit' => (int) $parameters['limit'],
            'size' => $count,
            'total' => $foundProducts->getTotalHits(),
            'results' => $results,
        ];
    }
}