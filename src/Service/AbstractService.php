<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use JoliCode\Elastically\Client;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Validation;

abstract class AbstractService
{
    protected FormFactoryInterface $formFactory;

    protected EntityManagerInterface $em;

    protected MessageBusInterface $bus;

    protected Client $elk;

    protected RequestStack $requestStack;

    /**
     * AbstractService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $em
     */
    public function __construct(
        RequestStack $requestStack,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $em,
        MessageBusInterface $bus,
        Client $elk
    ){
        $this->requestStack = $requestStack;
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->bus = $bus;
        $this->elk = $elk;
    }

    /**
     * Validate parameter from api requests
     *
     * @param array $parameters
     * @return ConstraintViolationList
     */
    public function validateParameters(array $parameters): ConstraintViolationList
    {
        $validator = Validation::createValidator();

        $constraintCollection = new Assert\Collection([
            'fields' => [
                'limit' => new Assert\Range(['min' => 0, 'max' => 999]),
                'offset' => [
                    new Assert\Type('numeric'),
                    new Assert\PositiveOrZero(),
                ],
                'name' => new Assert\Length(['min' => 1]),
                'latitude' => new Assert\Regex("/^(\-?([0-8]?[0-9](\.\d+)?|90(.[0]+)?)?)$/"),
                'longitude' => new Assert\Regex("/^(\-?([1]?[0-7]?[0-9](\.\d+)?|180((.[0]+)?)))$/"),
                'distance' => [
                    new Assert\Type('numeric'),
                    new Assert\Positive(),
                ],
                'stores' => new Assert\All([
                    new Assert\Positive(),
                    new Assert\Type('numeric'),
                ])
            ],
            'allowMissingFields' => true,
        ]);

        $constraintCallback = new Assert\Callback(
            ['callback' => static function (array $data, ExecutionContext $context) {

                if (!empty($data['latitude']) || !empty($data['longitude']) || !empty($data['distance'])) {
                    $message = 'If latitude, longitude or distance is set, they must all be setted !';
                    if(empty($data['latitude'])) {
                        $context
                            ->buildViolation($message)
                            ->atPath('[latitude]')
                            ->addViolation()
                        ;
                    }
                    if(empty($data['longitude'])) {
                        $context
                            ->buildViolation($message)
                            ->atPath('[longitude]')
                            ->addViolation()
                        ;
                    }
                    if(empty($data['distance'])) {
                        $context
                            ->buildViolation($message)
                            ->atPath('[distance]')
                            ->addViolation()
                        ;
                    }
                }
            }]
        );

        $violations = $validator->validate(
            $parameters,
            [
                $constraintCollection,
                $constraintCallback
            ]
        );

        return $violations;
    }

    /**
     * Get result links
     *
     * @param int $count count of returned results
     * @param array $parameters query parameters
     * @return array|null
     */
    protected function getLinks(int $count, array $parameters): null|array
    {
        if (!isset($parameters['offset'], $parameters['limit'])){
            return null;
        }
        $currentOffset = (int) $parameters['offset'];

        $request = $this->requestStack->getCurrentRequest();

        $links = [
            'self' => $request->getUri(),
        ];

        if ($count === (int) $parameters['limit']) {
            $parameters['offset'] = $currentOffset + 1;
            $links['next'] = $request->getScheme().'://'.$request->getHost().$request->getPathInfo().'?'.http_build_query($parameters);
        }

        if ($currentOffset > 0) {
            $parameters['offset'] = $currentOffset - 1;
            $links['prev'] = $request->getScheme().'://'.$request->getHost().$request->getPathInfo().'?'.http_build_query($parameters);
        }

        return $links;
    }
}