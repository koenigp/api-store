<?php

namespace App\Service;

use App\Entity\Manager;
use App\Form\ManagerType;

class ManagerService extends AbstractService
{
    public function addManager(array $managerData)
    {
        $manager = new Manager();
        $managerForm = $this->formFactory->create(ManagerType::class, $manager);
        $managerForm->submit($managerData);
        if (!$managerForm->isValid()) {
            return $managerForm;
        }

        $this->em->persist($manager);
        $this->em->flush($manager);

        $id = $manager->getId();

        return [
            'id' => $id,
        ];
    }
}