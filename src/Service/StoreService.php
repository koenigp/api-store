<?php

namespace App\Service;

use App\Entity\Store;
use App\Form\StoreType;
use Elastica\Query;
use JoliCode\Elastically\Messenger\IndexationRequest;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StoreService extends AbstractService
{
    public function addStore(array $storeData): array|Form
    {
        $store = new Store();
        $storeForm = $this->formFactory->create(StoreType::class, $store);
        $storeForm->submit($storeData);
        if (!$storeForm->isValid()) {
            return $storeForm;
        }

        $this->em->persist($store);
        $this->em->flush($store);

        $id = $store->getId();

        $this->bus->dispatch(new IndexationRequest(\App\Model\Store::class, $id));

        return [
            'id' => $id,
        ];
    }

    public function getStores(array $parameters): array|NotFoundHttpException
    {
        $es_bool = null;

        // apply name filter
        if (!empty($parameters["name"]))
        {
            $es_terms = new Query\Term(['name' => ['value' => $parameters['name']]]);
            $es_bool = new Query\BoolQuery();
            $es_bool->addMust($es_terms);
        }

        // apply geo_point filter
        if (!empty($parameters['latitude']) &&
            !empty($parameters['longitude']) &&
            !empty($parameters['distance'])
        ) {
            // query
            if (empty($es_bool)) {
                $es_bool = new Query\BoolQuery();
            }
            $es_distance = new Query\GeoDistance(
                'geo',
                [
                    'lat' => $parameters['latitude'],
                    'lon' => $parameters['longitude'],
                ],
                $parameters['distance']
            );
            $es_bool->addFilter($es_distance);

            // sort
            $sort = [
                '_geo_distance' => [
                    'geo' => [$parameters['longitude'], $parameters['latitude']],
                    'order' => 'asc',
                    'unit' => 'km'
                ]
            ];
        }

        $query = new Query($es_bool);
        if (isset($sort)) {
            $query->addSort($sort);
        }

        // Set offset and limit
        $query->setFrom($parameters['offset']);
        $query->setSize($parameters['limit']);

        $foundStores = $this->elk->getIndex('store')->search($query);
        // Count of documents returned by elk
        $count = $foundStores->count();
        if(0 === $count) {
            return new NotFoundHttpException();
        }

        $results = [];
        foreach ($foundStores->getResults() as $result) {
            /** @var \App\Model\Store $store */
            $store = $result->getModel();

            $row = [
                'id' => $store->id,
                'name' => $store->name,
                'address' => $store->address,
            ];

            if (!empty($store?->geo)) {
                $row['geo'] = [
                    'lat' => $store?->geo['lat'],
                    'lon' => $store?->geo['lon'],
                ];
            }

            if (!empty($store?->manager)) {
                $row['manager'] = [
                    'id' => $store?->manager['id'],
                    'first_name' => $store?->manager['first_name'],
                    'last_name' => $store?->manager['last_name'],
                ];
            }

            // Add distance from store if distance sort applied
            $hit = $result->getHit();
            if (!empty($hit['sort'][0])) {
                $row['distance'] = floor($hit['sort'][0]);
            }

            $results[] = $row;
        }

        return [
            '_links' => $this->getLinks($count, $parameters),
            'offset' => (int) $parameters['offset'],
            'limit' => (int) $parameters['limit'],
            'size' => $count,
            'total' => $foundStores->getTotalHits(),
            'results' => $results,
        ];
    }
}