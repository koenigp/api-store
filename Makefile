APP = docker exec -ti $$(docker-compose ps -q app)

all: start

install: rm install-vendor launch

start: stop-traefik start-traefik
	@docker-compose up --no-recreate -d app db myadmin elasticsearch kibana redis swagger

start-traefik:
	@docker-compose up -d traefik

# stop traefik if running and rm container
stop-traefik:
	@docker stop traefik || true && docker rm -f traefik || true

restart: stop start

stop:
	@docker-compose stop app myadmin db elasticsearch kibana redis swagger || true

kill:
	@docker-compose kill || true

rm: stop
	@docker-compose rm -f app db myadmin elasticsearch kibana redis swagger || true

install-vendor: start
	@$(APP) sh -c "composer install -n --prefer-dist --ignore-platform-reqs"

run: create-db

launch:
	@$(APP) bash -c "bin/console d:d:c && bin/console d:s:c && bin/console d:f:l -n && bin/console a:e:c && bin/console nelmio:apidoc:dump > openapi.json && bin/console messenger:consume async"
d-s-c:
	@$(APP) bash -c "bin/console d:s:c"
d-f-l:
	@$(APP) bash -c "bin/console d:f:l -n"
a-e-c:
	@$(APP) bash -c "bin/console a:e:c"
nelmio-doc:
	@$(APP) bash -c "bin/console nelmio:apidoc:dump > openapi.json"
worker:
	@$(APP) bash -c "bin/console messenger:consume async"

# composer
composer:
	@$(APP) bash -c "export http_proxy=${http_proxy} && export COMPOSER_MEMORY_LIMIT=-1 && composer ${ARGS}"

bash:
	@$(APP) bash

# app-console with args
console:
	@$(APP) bash -c "bin/console ${ARGS}"

cc:
	@$(APP) bash -c "bin/console c:c"

phpunit:
	@$(APP) bash -c "XDEBUG_MODE=coverage bin/phpunit ${ARGS}"

test-coverage:
	@$(APP) bash -c "XDEBUG_MODE=coverage bin/phpunit --coverage-html coverage"



