STORE API
=========

Requirements
------------

  * docker 17.06.0+

Installation
------------

#### With MAKE:

```bash
make install
```
#### Without MAKE:

```bash
docker-compose up --no-recreate -d traefik app db myadmin elasticsearch kibana redis swagger
docker exec -ti $(docker-compose ps -q app) sh -c "composer install -n --prefer-dist --ignore-platform-reqs"
docker exec -ti $(docker-compose ps -q app) sh -c "bin/console d:d:c"
docker exec -ti $(docker-compose ps -q app) sh -c "bin/console d:s:c"
docker exec -ti $(docker-compose ps -q app) sh -c "bin/console d:f:l -n"
docker exec -ti $(docker-compose ps -q app) sh -c "bin/console a:e:c"
docker exec -ti $(docker-compose ps -q app) sh -c "bin/console nelmio:apidoc:dump > openapi.json"
docker exec -ti $(docker-compose ps -q app) sh -c "bin/console messenger:consume async"
```

API DOC
-------

http://swagger.localhot

TEST & COVERAGE
---------------

#### With MAKE:

```bash
make test-coverage
```

#### Without MAKE:

```bash
docker exec -ti $(docker-compose ps -q app) bash -c "XDEBUG_MODE=coverage bin/phpunit --coverage-html coverage"
```

TOOLS
-----

http://phpmyadmin.localhost

http://kibana.localhost

postman collection file apistore.postman_collection.json 



